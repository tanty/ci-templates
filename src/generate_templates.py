#!/bin/env python3

import jinja2
import yaml
import re
import hashlib

from pathlib import Path


def sha256sum(paths):
    '''Returns the sha256sum of the file(s) at the given path(s) in the form
       sha256-123245deadbeef'''
    h = hashlib.new("sha256")
    for path in paths:
        with open(path, "rb") as fd:
            h.update(fd.read())
    return f'sha256-{h.hexdigest()}'


if __name__ == '__main__':
    config_data = {}

    env = jinja2.Environment(loader=jinja2.FileSystemLoader('./src'),
                             trim_blocks=True,
                             lstrip_blocks=True)
    env.globals["sha256sum"] = sha256sum

    # load the various definitions in the provided YAML files
    src_folder = Path('src')

    defaults_template = env.get_template('defaults.yml')
    globs = yaml.load(defaults_template.render({}),
                      Loader=yaml.Loader)['globals']

    special_files = ('defaults.yml')
    for distrib in [x for x in src_folder.iterdir()
                    if (x.name.endswith('.yml') and x.name not in special_files)]:
        with open(distrib) as fd:
            data = yaml.load(fd, Loader=yaml.Loader)

            # for each root element in each yaml file
            for config in data.values():

                # add the uppercase version of each key
                for key, value in [(k, v) for (k, v) in config.items()
                                   if isinstance(v, str)]:
                    config[key.upper()] = value.upper()

                # special case I could not figure out with defaults.yml
                if 'VERSION' in config:
                    del(config['VERSION'])

                # render the defaults template with the current root element
                defaults = yaml.load(defaults_template.render(config),
                                     Loader=yaml.Loader)['defaults']

                # add missing keys from the generated defaults
                for key, value in defaults.items():
                    if key not in config:
                        config[key] = value

                # add missing keys from the globals
                for key, value in globs.items():
                    if key not in config:
                        config[key] = value

            # store the generated values in the base config dictionary
            config_data.update(data)

    out_folder = Path('templates')
    out_folder.mkdir(exist_ok=True)

    ci_folder = Path('.gitlab-ci')
    ci_folder.mkdir(exist_ok=True)

    # define a generator for the list of scripts to be run in each distribution
    def get_script():
        n = 0
        while True:
            yield globs['scripts'][n]
            n += 1
            n %= len(globs['scripts'])

    scripts = get_script()

    # and render each distribution in the templates source directory
    for distrib, config in sorted(config_data.items()):
        # load our distribution template
        tmplfile = config.get('template') or 'template.tmpl'
        template = env.get_template(tmplfile)

        # and our ci template
        tmplfile = config.get('template-ci') or 'template-ci.tmpl'
        template_ci = env.get_template(tmplfile)

        dest = out_folder / f'{distrib}.yml'
        dest_ci = ci_folder / f'{distrib}-ci.yml'

        # use the next script for this config
        config['script'] = next(scripts)

        print(f'generating {dest}')
        with open(dest, 'w') as out_stream:
            template.stream(config).dump(out_stream)
        print(f'generating {dest_ci}')
        with open(dest_ci, 'w') as out_stream:
            template_ci.stream(config).dump(out_stream)

    # genereate the bootstrap file
    template_bootstrap = env.get_template('bootstrap.tmpl')
    dest = 'bootstrap/bootstrap.yml'
    print(f'generating {dest}')
    with open(dest, 'w') as out_stream:
        template_bootstrap.stream(globs).dump(out_stream)

    # We've generated all the templates, search for any
    # full image reference $registry/$path:$imagespec anywhere in your
    # templates and add that to the remote_images so we can test those
    # during the CI pipeline
    registry, path = config['ci_templates_registry'], config['ci_templates_registry_path']
    pattern = re.compile(f'^[^#]+({registry}{path}:[\\w:.-]+)')
    remote_images = []
    for template in Path('templates/').glob('*.yml'):
        for line in open(template):
            matches = pattern.match(line)
            if matches:
                remote_images.append(matches[1])

    # finally, regenerate the .gitlab-ci.yml
    template_general_ci = env.get_template('gitlab-ci.tmpl')

    # Note: we force ci-fairy to be last
    distro_names = sorted(config_data.keys(), key=lambda x: x if x != 'ci-fairy' else 'zzzz')
    distribs = [config_data[d] for d in distro_names if d not in ('defaults', 'globals')]
    dest = '.gitlab-ci.yml'

    config = {'distribs': distribs,
              'remote_images': sorted(set(remote_images))}
    config.update(globs)

    print(f'generating {dest}')
    with open(dest, 'w') as out_stream:
        template_general_ci.stream(config).dump(out_stream)
